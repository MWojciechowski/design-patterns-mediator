using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

// Building Web API's is quite easy with MediatR library. Check it out!
// Structure :
//                                          ------------| RequestQuery |    <- Input params
// | API Controller |<---->| Mediator |--|--| Handler |                     <- Receive messages from Mediator (business logic)
// (Handle HTTP Req)       (MediatR         ------------| Response |        <- Response messages
//                          object )                                      


namespace MarkerPositions
{
    static class Program
    {
        /// <summary>
        ///  The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.SetHighDpiMode(HighDpiMode.SystemAware);
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Form1());
        }
    }
}
