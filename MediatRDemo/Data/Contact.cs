﻿using System.ComponentModel.DataAnnotations;

namespace MediatRDemo
{
    public class Contact
    {
        [Key]
        public int ContextId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
    }
}