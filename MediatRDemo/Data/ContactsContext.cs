﻿using Microsoft.EntityFrameworkCore;

namespace MediatRDemo
{
    public class ContactsContext : DbContext
    {
        public ContactsContext(DbContextOptions<ContactsContext> options) : base(options)
        {
        }

        public DbSet<Contact> Contacts { get; set; }

        protected override void OnModelCreating(ModelBuilder mb)
        {
            mb.Entity<Contact>().HasData(
                new Contact { ContextId = 1, FirstName = "Steve", LastName = "Stevenson" },
                new Contact { ContextId = 2, FirstName = "Bill", LastName = "Billinson" },
                new Contact { ContextId = 3, FirstName = "Anja", LastName = "Bjergsen" });
        }
    }
}