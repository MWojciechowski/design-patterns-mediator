using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;

// Building Web API's is quite easy with MediatR library. Check it out!
// Structure :
//                                          ------------| RequestQuery |    <- Input params
// | API Controller |<---->| Mediator |--|--| Handler |                     <- Receive messages from Mediator (business logic)
// (Handle HTTP Req)       (MediatR         ------------| Response |        <- Response messages
//                          object )                                      


namespace MediatRDemo
{
    public class Program
    {
        public static void Main(string[] args)
        {
            CreateHostBuilder(args).Build().Run();
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder.UseStartup<Startup>();
                });
    }
}
